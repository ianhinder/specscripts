<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="state">
    <span class="{@class}"><xsl:value-of select="."/></span>
  </xsl:template>

  <xsl:template match="status">
    <span class="{translate(text(), 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')}">
      <xsl:value-of select="."/>
    </span>
  </xsl:template>

  <xsl:template match="name">
    <a href="{@href}"><xsl:value-of select="."/></a>
  </xsl:template>

  <xsl:template match="simulations">
    <table>
      <tr>
        <xsl:for-each select="fields/field">
          <th><xsl:value-of select="."/></th>
        </xsl:for-each>
      </tr>
      <xsl:for-each select="simulation">
        <tr>
          <xsl:for-each select="*">
            <td><xsl:apply-templates select="."/></td>
          </xsl:for-each>
        </tr>
      </xsl:for-each>
    </table>
  </xsl:template>

  <xsl:template match="/">
    <html>
      <head>
        <link rel="StyleSheet" href="monitoring.css" type="text/css"/>
        <title>Simulations</title>
      </head>
      <body>
        <xsl:apply-templates/> 
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
