<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:include href="status.xsl"/>

  <xsl:template match="plots">
    <html>
      <head>
        <title><xsl:value-of select="title"/></title>
        <link rel="StyleSheet" href="monitoring.css" type="text/css"/>
      </head>
      <body>
        <h1><xsl:value-of select="title"/></h1>

        <xsl:variable name="plotfile" select="plotfile"/>
        <xsl:for-each select="document('index.xml')/simulations/simulation">
          <div class="plot">
            <a href="./{.}/index.xml"><h3><xsl:value-of select="."/></h3></a>
            <p>
              <xsl:for-each select="document('status.xml')/simulations/simulation[contains(name, current()/text())]/status">
                <xsl:apply-templates select="."/>
                <xsl:if test="position() != last()">
                  <xsl:text>, </xsl:text>
                </xsl:if>
              </xsl:for-each>
            </p>
            <img src="./{.}/{$plotfile}"/>
          </div>
        </xsl:for-each>
      </body>
    </html>
  </xsl:template>

</xsl:stylesheet>
