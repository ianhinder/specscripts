<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:include href="status.xsl"/>

<xsl:variable name='projectname' select="document('config.xml')/simulationsconfig/projectname"/>


  <!-- <xsl:template match="date"> -->
  <!--   <xsl:value-of select="."/> -->
  <!-- </xsl:template> -->

  <!-- <xsl:template match="problems"> -->
  <!--   <pre> -->
  <!--     <xsl:value-of select="."/> -->
  <!--   </pre> -->
  <!-- </xsl:template> -->

  <!-- <xsl:template match="statustable"> -->
  <!-- </xsl:template> -->

  <xsl:template match="/">

    <html>
      <head>
        <link rel="StyleSheet" href="monitoring.css" type="text/css"/>
        <title><xsl:value-of select="$projectname"/></title>
      </head>

      <body>

      <h1><xsl:value-of select="$projectname"/></h1>

      <pre><xsl:apply-templates select="document('monitor-problems.xml')/monitorproblems"/></pre>
      <p>Page last updated: <xsl:apply-templates select="document('last-update.xml')/date"/></p>

      <h2>Status</h2>
      <!-- <p><img src="status.svg" align="top"/></p> -->
      <xsl:apply-templates select="document('status.xml')/simulations"/>
      <!-- <xsl:copy-of select="document('status.xml')"/> -->
      <!-- <pre><xsl:apply-templates select="document('problems.xml')/problems"/></pre> -->

      <h2>Remaining run time</h2>
      <p><img src="remaining.svg" align="top"/></p>

      <ul>
        <xsl:for-each select="document('summaryplots.xml')/summaryplots/plotpage"> 
          <li style="display: inline-block; padding-right: 25px;">
            <a href="{@href}"><xsl:value-of select="document(@href)/plots/title"/></a>
          </li>
        </xsl:for-each>
      </ul>

      <xsl:copy-of select="document('errors.txt')/errors"/>

      <xsl:for-each select="simulations/simulation">
        <xsl:copy-of select="document(concat(current(),'/errors.xml'))/errors"/>
      </xsl:for-each>

      </body>

    </html>
  </xsl:template>
</xsl:stylesheet>
