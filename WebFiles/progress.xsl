<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/simulations">
          <xsl:for-each select="simulation">
            <div class="plot">
              <a href="./{.}/index.xml"><h3 style="word-wrap:break-word;"><xsl:value-of select="."/></h3></a>
              <img src="./{.}/progress.svg" />
            </div>
          </xsl:for-each>
  </xsl:template>

  <xsl:template match="plots">


    <html>
      <head>
        <title>Progress</title>
        <style type="text/css">

    table {
    font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
    font-size: 10pt;
    background: #fff;
    margin: 10px;
    width: 90%; 
    border-collapse: collapse;
    text-align: left;
    }
    
    th {
    font-size: 14px;
    font-weight: bold;
    padding: 10px 0px;
    text-align: left;
    }

    th.date {
    width : 17ex;
    }

tr
{
border-bottom: 1px solid #ccc; 
padding: 8px 8px;
}

td
{
padding: 4px;
}

.stopped
{
   color: red;
}

.finished
{
   color: lightgray;
}

.running
{
   color: green;
}

div.plot {
width: 300px;
margin: 0px;
padding: 10px;
display: inline-block;
vertical-align: top;
}

a
{
text-decoration: none;
color: black;

}
        </style>
      </head>

      <body>
        <h1>Progress</h1>
        <xsl:apply-templates select="document('index.xml')"/>
      </body>

    </html>
  </xsl:template>


</xsl:stylesheet>
