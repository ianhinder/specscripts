<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">


<!-- <xsl:variable name='setname' select="document('config.xml')/simulationsconfig/setname"/> -->


  <!-- <xsl:template match="date"> -->
  <!--   <xsl:value-of select="."/> -->
  <!-- </xsl:template> -->

  <!-- <xsl:template match="problems"> -->
  <!--   <pre> -->
  <!--     <xsl:value-of select="."/> -->
  <!--   </pre> -->
  <!-- </xsl:template> -->

  <!-- <xsl:template match="statustable"> -->
  <!-- </xsl:template> -->

  <xsl:template match="/">

    <html>
      <head>
        <title><xsl:value-of select="simulation/name"/></title>
        <link rel="StyleSheet" href="monitoring.css" type="text/css"/>
      </head>

      <body>

      <h1><xsl:value-of select="simulation/name"/></h1>

      <p>Last updated: <xsl:value-of select="simulation/lastupdated"/></p>

      <!-- <pre><xsl:apply-templates select="document('monitor-problems.xml')/monitorproblems"/></pre> -->
      <!-- <p>Page last updated: <xsl:apply-templates select="document('last-update.xml')/date"/></p> -->

      <!-- <p><img src="status.svg" align="top"/></p> -->
      <!-- <xsl:copy-of select="document('status.html')/table"/> -->

      <!-- <pre><xsl:apply-templates select="document('problems.xml')/problems"/></pre> -->

      <!-- <p> -->
      <!--   <img src="speed.png"/> -->
      <!--   <img src="progress.png"/> -->
      <!--   <img src="memory.png"/> -->
      <!-- </p> -->

      <table>
        <!-- <xsl:for-each select="simulations/simulation"> -->
          <!-- <tr><td colspan="1"><h2><xsl:value-of select="."/></h2></td></tr> -->
          <tr>
            <!-- <td><img src="{.}/ecc.png"/></td> -->
            <!-- <td><img src="{.}/psi4s.png"/></td> -->
            <!-- <td><img src="{.}/traj.png"/></td> -->
            <!-- <td><img src="{.}/phase-errs.png"/></td> -->
            <td>
              <p>
                <img src="params.svg" class="plot"/>
                <img src="id.svg" class="plot"/>
                <img src="ecc.svg" class="plot"/>
                <img src="memory.svg"  class="plot"/>
                <img src="subdomains.svg" class="plot" />
                <img src="gridpoints.svg" class="plot" />
                <img src="cores.svg" class="plot" />
                <img src="speed.svg"  class="plot"/>
                <img src="progress.svg"  class="plot"/>
                <img src="hs.svg" class="plot"/>
                <img src="h_om.svg" class="plot"/>
                <img src="traj.svg" class="plot"/>
                <img src="sep.svg" class="plot"/>
                <img src="om.svg" class="plot"/>
                <img src="phase-errs.svg" class="plot"/>
              </p>
              <br/>
            </td>
          </tr>
        <!-- </xsl:for-each> -->
      </table>

      <xsl:copy-of select="document('errors.xml')/errors"/>

      </body>

    </html>
  </xsl:template>
</xsl:stylesheet>
