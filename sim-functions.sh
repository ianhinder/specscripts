#!/bin/bash

source ./specsims.conf

function sim_base_dir()
{
    echo $SIM_BASE_DIR
}

# Should we return absolute paths or not? Yes for now.
function sim_segments()
{
    local spec=($(parse_sim_spec "$1")) || return

    simdir=$(find_sim ${spec[0]}) || return

    if [ $# = 1 ]; then
        # No segments in this spec; this is a directory only
        # Output any valid subdirectories which can be considered as simulations
# #$simdir/ID $simdir/Ev
#         for f in "/lustre/datura/ianhin/simulations/q1_o8_e0"/Ecc*; do
#             echo $f;
#         done

        # ls $simdir

        # ls ${simdir}/Ecc* $simdir/ID $simdir/Ev

        for subseg in $(ls -d $simdir/Ecc* $simdir/ID $simdir/Ev 2>/dev/null); do
            echo ${subseg##*/}
        done

    fi


#     local simdir=$(find_sim "$1")

#     local segdirs=$(ls -d $simdir/Ecc? $simdir/ID $simdir/Ev 2>/dev/null)

# #    segdirs=$(segdirs//_AA//)



#     local evdir=$simdir/Ev

#     ls -d $evdir/Lev${lev}_?? $evdir/Lev${lev}_Ringdown/Lev${lev}_?? 2>/dev/null
}

function parse_sim_spec()
{
    local fields
    IFS=":" read -ra fields <<< "$1"
    echo "${fields[*]}"
}

function string_riffle()
{
    local IFS="$1"; shift; echo "$*"
}

function xfind_sim_dir()
{
    local simspec=($(parse_sim_spec "$1"))

#    echo "${simspec[@]}"

    if [[ ${simspec[0]} =~ ^/|~ ]]; then
        toppath=${simspec[0]}
    else
        toppath="$(sim_base_dir)/${simspec[0]}"
    fi

    # if [ ! -r $toppath ]; then
    #     echo "Cannot find directory $toppath for simulation $1" >&2
    #     return 1
    # fi

#    echo "${simspec[1]}"

    if [ ${#simspec[@]} = 1 ]; then
        # There are no segments in the simspec, so return the
        # directory part of the top of the simulation
        echo $toppath
    elif [[ "${simspec[1]}" =~ ^Lev.* ]]; then
        # echo 1
        if [ ${#simspec[@]} = 2 ]; then
            echo "It is not possible to determine a directory for a resolution with SpEC" >&2
            return 1
        else
            # echo $toppath
            # echo "$toppath / ${simspec[1]} _ ${simspec[2]}"

            thispath=$(find_sim_dir "$toppath/${simspec[1]}_${simspec[2]}")
            if [ ${#simspec[@]} = 3 ]; then
                echo $thispath
            else
                echo ${thispath}:$(string_riffle ":" "${simspec[@]:3}")
            fi
        fi
    else
        # echo 2
        thispath=$toppath/${simspec[1]}
        if [ ${#simspec[@]} = 2 ]; then
            echo $thispath
        else
            echo $thispath:$(string_riffle ":" "${simspec[@]:2}")
        fi
    fi
}

# Given the name of a simulation (not including a resolution), return
# a full path to a directory representing the simulation, i.e. one
# that contains an Ev directory.  Segments are not considered
# simulations.
function find_sim_dir()
{
    if [ $# != 1 ]; then
        echo "find_sim_dir requires one argument" >&2
        exit 1
    fi

    local sim=$1
    local path

    if [[ $sim =~ ^/|~ ]]; then
        path=$sim
    else
        path=$(sim_base_dir)/$sim
    fi
    echo $path
}

# Given a path to a simulation, shorten its name if it is prefixed
# with the simulation path
function find_sim_name()
{
    if [ $# != 1 ]; then
        echo "find_sim_name requires one argument" >&2
        exit 1
    fi

    local path=$1
    local sim

    if [[ $path =~ ^$(sim_base_dir) ]]; then
        sim=${path#$(sim_base_dir)/}
    else
        sim=$path
    fi
    echo $sim
}

function find_sim_levels()
{
    if [ $# != 1 ]; then
        echo "find_sim_levels requires one argument" >&2
        exit 1
    fi

    if ! shopt -q nullglob; then
        echo "find_sim_levels requires shopt nullglob" >&2
        exit 1
    fi

    local sim=$1
    local path=$(find_sim_dir $sim)
#    echo " path = $path" >&2
    local level_dirs=(${path}/Ev/Lev?_AA)
#    echo " level_dirs = ${level_dirs[*]}" >&2
    local tmp
    local levels=($(for lp in ${level_dirs[*]}; do tmp=${lp#${path}/Ev/Lev}; echo ${tmp%_AA}; done))
    echo ${levels[*]}
}

# Given the name of a simulation, or the path to the directory, and a
# level, return paths to all the segments in that level
function find_sim_segments()
{
    if [ $# != 2 ]; then
        echo "find_sim_segments requires two arguments" >&2
        return 1
    fi

    if ! shopt -q nullglob; then
        echo "find_sim_segments requires shopt nullglob" >&2
        return 1
    fi

    local sim=$1
    local lev=$2
    local simpath=$(find_sim_dir $sim)
    local segment_dirs=(${simpath}/Ev/Lev${lev}_?? ${simpath}/Ev/Lev${lev}_Ringdown/Lev${lev}_??)
    if [ ${#segment_dirs[*]} = 0 ]; then
        echo "Level $lev not found in simulation $sim" >&2
        return 1
    fi
    for segdir in ${segment_dirs[*]}; do echo ${segdir#${simpath}/}; done
}

function find_last_sim_segment()
{
    if [ $# != 2 ]; then
        echo "find_last_sim_segment requires two arguments" >&2
        return 1
    fi

    if ! shopt -q nullglob; then
        echo "find_last_sim_segment requires shopt nullglob" >&2
        return 1
    fi

    local sim=$1
    local lev=$2

    local segments=($(set -e; find_sim_segments $sim $lev))
    local nsegments=${#segments[*]}
    echo ${segments[${#segments[*]}-1]}
}


# List the simulations that we want to track
function sim_list()
{
    if [ $# != 0 ]; then
        echo "sim_list takes no arguments" >&2
        exit 1
    fi

    local topsim
    local givenlev
    local keywords

    if [ -r "active-sims.txt" ]; then
        active_sims="active-sims.txt"
    else
        active_sims="active-sims.auto.txt"

        for f in $(sim_base_dir)/*/*/DoMultipleRuns.input; do
            sim_dir=${f%/*/DoMultipleRuns.input}
            sim_name=${sim_dir#$(sim_base_dir)/}
            echo $sim_name >>${active_sims}.tmp
        done
        sort -u ${active_sims}.tmp > ${active_sims}
        rm -f ${active_sims}.tmp
    fi

    while read topsim givenlev keywords; do
        if [ "$topsim" = "#" -o -z "$topsim" ]; then
            continue
        fi
        
        local topsim_path=$(set -e; find_sim_dir $topsim)

        if [ ! -d $topsim_path ]; then
            echo "Simulation $topsim not found ($topsim_path)" >&2
            return 1
        fi

        # Given a base simulation name, loop over any
        # eccentricity-reduction simulations
        local all_sims=($topsim_path $topsim_path/Ecc*)

        all_sims=($(for sim in ${all_sims[@]}; do echo $sim/ID* $sim; done))

        all_sims=($(for sim in ${all_sims[@]}; do find_sim_name $sim; done))

        local sim
        n=$((${#all_sims[@]}-1))
        for sim in ${all_sims[$n]}; do
            local id_pattern='/ID$'
            if [[ "$sim" =~ $id_pattern ]]; then
                echo $sim 0
            elif [ -d $(find_sim_dir $sim)/Ev ]; then
                local levels
                if [ -z $givenlev ]; then
                    levels=($(find_sim_levels $sim))
                else
                    levels=($givenlev)
                fi
                for lev in ${levels[*]}; do
                    echo $sim $lev $keywords
                done
            else
                echo "Simulation $sim is of unrecognised type" >&2
                return 1
            fi
        done
    done < $active_sims
}

function job_id_from_work_dir()
{
    if [ $# != 1 ]; then
        echo "job_id_from_work_dir requires one argument" >&2
        exit 1
    fi

    local work_dir=$1

    # There is no job id stored for the last segment.  It might be in
    # the queue or it might have been in the queue and then deleted.
    # Need to scan all jobs in the queue to find it.
    local job_id="none"
    alljobids=$(qstat | tail -n +3 | awk '{print $1}')
    for j in $alljobids; do
        job_work_dir="$(qstat -j $j |grep sge_o_workdir|awk '{print $2}')"
        if [ $work_dir = $job_work_dir ]; then
            job_id=$j
            break
        fi
    done
    echo $job_id
}


function sim_job_id()
{
    local sim=$1
    local lev=$2

    if [ $# != 2 ]; then
        echo "sim_job_id requires two arguments" >&2
        exit 1
    fi

    local simpath=$(set -e; find_sim_dir $sim)

    local id_pattern='/ID$'
    local job_id="none"
    if [[ "$simpath" =~ $id_pattern ]]; then
        # This is an ID simulation
        local env_file=$simpath/private/bin/env.log
        if [ -r $env_file ]; then
            local job_id_line=$(set -e; grep 'JOB_ID=' $env_file)
            job_id=${job_id_line#*=}
        else 
            # There is no env file, but the job might be in the queue
            job_id=$(set -e; job_id_from_work_dir ${simpath%/ID})
        fi
        # Might want to eliminate this, so the caller can check for it
        if [ "$job_id" = "none" -o -z "$job_id" ]; then
#            echo "Cannot find job id in $sim" >&2
#            return 1
            echo "none"
        fi
    elif [ -r $simpath/Ev ]; then
        # This is an evolution simulation
        local evdir=$simpath/Ev

        # TODO: Maybe we can also treat segments as simulations?
        last_segment=$(set -e; find_sim_segments $sim $lev|tail -1)
        job_id_file=$simpath/$last_segment/SpEC.jobid
        if [ -r $job_id_file ]; then
            job_id=$(set -e; tail -1 $job_id_file|awk '{print $1}')
        else
            # There is no job id file, but the job might be in the
            # queue
            job_id=$(set -e; job_id_from_work_dir ${simpath}/$last_segment)
        fi
    fi
    if [ -z "$job_id" ]; then
        job_id="none"
        # echo "Error determining job id for $sim $lev" >&2
        # exit 1
    fi
    echo $job_id
}

function sim_termination_reason()
{
    local sim=$1
    local lev=$2

    if [ $# != 2 ]; then
        echo "sim_termination_reason requires two arguments" >&2
        exit 1
    fi

    local simpath=$(set -e; find_sim_dir $sim)
    local last_seg=$(set -e; find_last_sim_segment $sim $lev)
    local seg_path=$simpath/$last_seg

    term_file=$seg_path/Run/TerminationReason.txt

    if [ -r "$term_file" ]; then
        term_reason=$(grep 'Termination condition' $term_file)
        term_reason=${term_reason#'Termination condition '}
        echo "$term_reason"
    elif [ ! -r $seg_path/Run ]; then
        echo "NotYetRun"
    else
        echo "None"
    fi
}

function sim_termination_state()
{
    local sim=$1
    local lev=$2

    if [ $# != 2 ]; then
        echo "sim_termination_state requires two arguments" >&2
        exit 1
    fi

    local term_reason=$(sim_termination_reason $sim $lev)

    continue_pattern="FileExists|CommonHorizon|FoshRhsIsNotBalanced|MemoryIsNotBalanced|PreserveRelativeDeltaRA|PreserveRelativeDeltaRB|SplitOrMergeSubdomains|WallClock"
    stop_pattern="FinalTime|IngoingCharFieldOnSphericalBdry|ProportionalIntegral::TerminationTolerance"

    if [[ "$term_reason" =~ $continue_pattern ]]; then
        echo "Continue"
    elif [[ "$term_reason" =~ $stop_pattern ]]; then
        echo "Stop"
    elif [[ "$term_reason" = NotYetRun ]]; then
        echo "NotYetRun"
    elif [[ "$term_reason" = None ]]; then
        echo "None"
    else
        echo "Simulation $sim terminated with unsupported reason \"${term_reason}\"" >&2
        exit 1
    fi
}
