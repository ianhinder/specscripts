#!/home/ianhin/software/python-2.7.3/bin/python

import xml.etree.ElementTree
import sys

in_file = sys.argv[1]
out_file = sys.argv[2]

tree = xml.etree.ElementTree.parse(in_file)
elem = tree.getroot()

if 'width' in elem.attrib and 'height' in elem.attrib:
    print "Has size already"
    sys.exit(0)
else:
    print "No size information"

print "Searching for rectangles"

for rect in elem.iter('{http://www.w3.org/2000/svg}rect'):
    width = rect.attrib['width']
    height = rect.attrib['height']

    print width, height

elem.attrib['width'] = width
elem.attrib['height'] = height

# , default_namespace='http://www.w3.org/2000/svg'
tree.write(out_file, xml_declaration=True)
