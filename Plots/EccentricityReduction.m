(*********************************************************************
  Eccentricity reduction
 *********************************************************************)

 eccentricityReductionPlots[sim_String] :=
    Module[{eccRedSims,eccRedSims2, eccs, eccRads, legends, plot},

    Print["ecc.svg: ", sim];

      eccRedSims = FindSpECEccentricityReductionSimulations[sim];

      plot =
      If[eccRedSims === {}, $nullPlot,

      levs = FindSpECLevels/@eccRedSims;

      eccRedSims = Pick[eccRedSims, Map[(# =!= {}) &, levs]];

      eccRedSims2 =
      MapThread[
        StringJoin[#1, ":", #2] &, {eccRedSims,
          ToString /@ Last /@ FindSpECLevels /@ eccRedSims}];
      
      eccRads = Norm/@readHorizon/@eccRedSims2;

      have = Map[Length[#]>1&, eccRads];

      eccRedSims = Pick[eccRedSims, have];
      eccRedSims2 = Pick[eccRedSims2, have];
      eccRads = Pick[eccRads, have];

      eccs = ReadSpECEccentricity/@eccRedSims;
      
      legends = Map[If[#===None, "", SequenceForm["e = ",ScientificForm[#,2]]] &, eccs];

      PresentationListLinePlot[eccRads, plotOptions, PlotRange -> {0,All},
        PlotLegend -> legends (*(FileNameTake[#,-1] & /@ eccRedSims) *),
        AspectRatio -> 1,
        LegendPosition -> {Left, Bottom}, FrameLabel -> {"t/M", "r/M"},
        PlotLabel -> "Eccentricity reduction"]];
      
      {{FileNameJoin[{"web-simulations",sim,"ecc.svg"}], plot}}];
