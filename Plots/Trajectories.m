trajectoryPlots[sim_String] :=
  Module[{levels, sims, horizons, trajectories, xs, ys, xRange, yRange, range, plot, have},
    
    Print["traj.svg: ", sim];
    
    {labels,sims} = findSimulationsToPlot[sim];

    plot = 
    If[sims === {}, $nullPlot,
        
    horizons = Map[(readHorizon[#]) &, sims];

    have = Map[Length[#[[1]]] > 0 &, horizons];

    horizons = Pick[horizons, have];
    labels = Pick[labels, have];

    trajectories = Map[Thread[ToListOfData /@ #[[1 ;; 2]]] &, horizons];

    xs = Map[First, trajectories, {2}];
    ys = Map[Last, trajectories, {2}];

    xRange = {Min[xs], Max[xs]};
    yRange = {Min[ys], Max[ys]};

    range = Max[Abs/@xRange, Abs/@yRange] /. Infinity -> 100;

    PresentationListLinePlot[trajectories, plotOptions, PlotRange -> 1.1 range {{-1,1},{-1,1}},
      PlotLabel-> "Trajectory",
      AspectRatio -> Automatic,
      PlotLegend -> labels,
      FrameLabel ->{"x/M", "y/M"}]];

    {<|"Filename" -> FileNameJoin[{"web-simulations",sim,"traj.svg"}], "Plot" -> plot,
      "Title" -> "Trajectories"|>}];
