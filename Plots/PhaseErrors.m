(*********************************************************************
  Phase errors
 *********************************************************************)

phaseErrorPlots[sim_String] :=
  Module[{sims, psi4s, choose, phases, minLength, phaseErrs, freq,
    horizons, sep, tRef, legend, plot, shiftSlab, align},

    Print["phase-errs.svg: ", sim];

    (* Print["levels = ", levels = FindSpECLevels[sim]]; *)
    {labels,sims} = findSimulationsToPlot[sim];

    If[Length[sims] < 2,
      plot = $nullPlot,
      (* else *)
      sims = Select[sims, ReadSpECPsi4Radii[#] =!= {} &];
      shiftSlab[d_DataTable] :=
      Slab[Shifted[d,-100],0;;All];
      psi4s = readPsi4 /@ sims;
      choose = Map[MaxCoordinate[#]>110 &, psi4s];
      psi4s = Pick[psi4s, choose];
      If[Length[psi4s] < 2,
        plot = $nullPlot,
        (* else *)
        sims = Pick[sims, choose];
        psi4s = shiftSlab /@ psi4s;
        phases = Phase /@ psi4s;
        (* Indices of the phases which are long enough to align by multiples of 2 Pi *)
        align = Pick[Range[1,Length[phases]], MaxCoordinate/@phases, l_ /; l > 200];
        (* Align those phases which can be aligned *)
        phases[[align]] = AlignPhases[phases[[align]], 200];
        phaseErrs = MapSuccessive[subtract, phases];
        freq = Derivative[1][Interpolation[phases[[1]]]];
        sep = Norm[readHorizon[sims[[1]]]];

        (* FindRoot[x^2 == 0, {x, 5, 2, 7}]; *)

        (* For some reason, the combination of the
           InterpolatingFunction::dmval message, the Quiet, and
           AbortOnMessagesST, causes Message::msgl, which is not
           printed, but is caught by MessageCatcher and causes an
           Abort which is very difficult to track down.  Weird. *)

        AbortOnMessagesST[False];
        If[Length[sep] === 0,
          tRef=None,
          (* else *)
          tRef = If[Last[sep] < 2,
            tRefGuess = MaxCoordinate[sep];
            (* Print["$MessageList = ", $MessageList]; *)
            Quiet[
              t /. FindRoot[freq[t] == -0.2, {t, tRefGuess}],
              {InterpolatingFunction::dmval}],
            (* else *)
            None]];
        (* TODO: only re-enable if it was enabled to start with *)
        AbortOnMessagesST[True];

        ShowIt[tRef];

        phaseRefErrs =
        If[tRef =!= None,
          If[MaxCoordinate[#]>tRef, Interpolation[#][tRef], None] & /@ phaseErrs,
          None];

        legend = MapSuccessive[
          (ToString[Subscript["\[Phi]", labels[[#1+1]]], TraditionalForm] <> "-" <> 
            ToString[Subscript["\[Phi]", labels[[#2+1]]], TraditionalForm] <> 
            If[tRef =!= None && phaseRefErrs[[#+1]] =!= None, " = "<>ToString[NumberForm[phaseRefErrs[[#+1]],{Infinity,4}]],""]) &, 
          Range[0, Length[phases] - 1]];
        plot = PresentationListLinePlot[Log10 /@ Abs /@ phaseErrs, plotOptions,
          FrameLabel -> {"t/M", 
             "\!\(\*SubscriptBox[\(log\), \(10\)]\)|\[CapitalDelta]\[Phi]|"}, 
          PlotLegend -> legend, LegendPosition -> {Left, Top},
          GridLines -> {If[tRef=!=None,{tRef},None], None}, Axes -> None, 
          AspectRatio -> 1,
          PlotRange -> {{0, If[tRef=!=None,tRef+150,All]}, {-6, 2}},
          PlotLabel -> "Phase error"]]];

    {{FileNameJoin[{"web-simulations",sim,"phase-errs.svg"}], plot}}];

