parameterPlots[sim_String] :=

  Module[{sims, formatVec, fixedParams, cleanSim, variableParams, idSubSimNames, plot,
    chiA, chiB},
    Print["params.svg: ", sim];

    sims = 
    If[StringMatchQ[sim, ___~~"Ecc"~~_~~EndOfString],
      {sim},
      FileNames["Ecc*", FindSpECSimulation[sim]]];

    If[sims === {}, sims = {sim}];

    formatVec[{x_,y_,z_}] :=
    SequenceForm["(",x,", ",y,", ",z,")"];

    paramSim = sims[[-1]];

    If[!HaveSpECInitialDataParameters[paramSim],
      Return[{{FileNameJoin[{"web-simulations",sim,"params.svg"}], $nullPlot}}]];

    chiA = Chop@ReadSpECInitialDimensionlessSpin[paramSim,1];
    chiB = Chop@ReadSpECInitialDimensionlessSpin[paramSim,2];

    fixedParams = Chop[#,10^-4] & @{
      {"q", ReadSpECInitialDataParameter[sims[[1]], "MA"]/ReadSpECInitialDataParameter[paramSim, "MB"]},
      {HoldForm[Subscript["\[Chi]", "A"]], Norm@chiA},
      {HoldForm[Subscript["\[Chi]", "B"]], Norm@chiB},
      {HoldForm[Subscript[OverVector["\[Chi]"], "A"]], formatVec@chiA},
      {HoldForm[Subscript[OverVector["\[Chi]"], "B"]], formatVec@chiB},
      {"D/M",NumberForm[ReadSpECInitialSeparation[paramSim],5]},
      {HoldForm["M" Subscript["\[CapitalOmega]",0]], ReadSpECInitialOrbitalFrequency[paramSim]},
      {HoldForm[Subscript[OverDot["a"],0]], ScientificForm@NumberForm[ReadSpECInitialDataParameter[paramSim, "adot0"],3]}};

    cleanSim[ss_String] :=
    StringReplace[ss,  ___ ~~ sim ~~ "/" :> ""];

    idSubSimNames =
    Map[StringReplace[#, __ ~~ sim ~~ "/" -> ""] &, sims];

    plot = Graphics[Inset[Grid[fixedParams, ItemStyle -> 8, Dividers->All, Alignment -> Left, ItemSize -> {{Scaled[0.3],Scaled[0.7]}}],{0,1.0},Top,{2,2}], plotOptions, AspectRatio -> 1, PlotLabel -> "Parameters",PlotRange->{{-1,1},{-1,1}}, Frame->False];

    {{FileNameJoin[{"web-simulations",sim,"params.svg"}], plot}}];
