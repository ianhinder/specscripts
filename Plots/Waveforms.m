(*********************************************************************
  Waveforms
 *********************************************************************)

waveformPlots[sim_String] :=
  Module[{sims, hs, plot},
    
    Print["hs.svg: ", sim];

    Print["levels = ", levels = FindSpECLevels[sim]];

    {labels,sims} = findSimulationsToPlot[sim];

    plot = 
    If[sims === {}, $nullPlot,
    
    sims = Select[sims, ReadSpECPsi4Radii[#] =!= {} &];
    hs = Map[(print[#]; readStrain[#])&, sims[[All]]];

    PresentationListLinePlot[Re /@ hs, plotOptions, PlotRange -> {{0, All}, All},
      PlotLegend -> labels, PlotLabel-> "Waveform",
      AspectRatio -> 1,
      FrameLabel ->{"t/M", "Re[h22]"}]];

    {<|"Filename" -> FileNameJoin[{"web-simulations",sim,"hs.svg"}], "Plot" -> plot,
      "Title" -> "Waveform"|>}];
