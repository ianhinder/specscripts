(*********************************************************************
  Waveforms
 *********************************************************************)

waveFrequencyPlots[sim_String] :=
  Module[{sims, hs, plot},
    
    levels = FindSpECLevels[sim];

    {labels,sims} = findSimulationsToPlot[sim];

    plot = 
    If[sims === {}, $nullPlot,
    
    sims = Select[sims, ReadSpECPsi4Radii[#] =!= {} &];
    hs = Map[(print[#]; readStrain[#])&, sims[[All]]];
    oms = Frequency /@ hs;

    PresentationListLinePlot[Log10/@Abs/@oms, plotOptions, PlotRange -> {{0, All}, {-2,0}},
      PlotLegend -> labels, PlotLabel-> "Waveform frequency",
      AspectRatio -> 1,
      FrameLabel ->{"t/M", "log10(abs(om_22))"}]];

    {<|"Filename" -> FileNameJoin[{"web-simulations",sim,"h_om.svg"}], "Plot" -> plot,
      "Title" -> "Waveform frequency"|>}];
