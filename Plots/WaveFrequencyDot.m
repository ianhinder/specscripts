(*********************************************************************
  Waveforms
 *********************************************************************)

waveFrequencyDotPlots[sim_String] :=
  Module[{sims, hs, plot, omDots, oms},
    
    levels = FindSpECLevels[sim];

    {labels,sims} = findSimulationsToPlot[sim];

    plot = 
    If[sims === {}, $nullPlot,
    
    sims = Select[sims, ReadSpECPsi4Radii[#] =!= {} &];
    hs = Map[(print[#]; readStrain[#])&, sims[[All]]];
    oms = Frequency /@ hs;
    omDots = NDerivative[1] /@ oms;

    PresentationListLinePlot[omDots, plotOptions, PlotRange -> {{0, All}, {-1,1} 10^-4},
      PlotLegend -> labels, PlotLabel-> "Waveform frequency time derivative",
      AspectRatio -> 1,
      FrameLabel ->{"t/M", "d/dt om_22"}]];

    {<|"Filename" -> FileNameJoin[{"web-simulations",sim,"h_omDot.svg"}], "Plot" -> plot,
      "Title" -> "Waveform frequency time derivative"|>}];
