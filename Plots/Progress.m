(*********************************************************************
  Progress
 *********************************************************************)

progressPlots[sim_String] :=
  Module[{sims, plot, labels, progress, have},

    Print["progress.svg: ", sim];

    {labels,sims} = findSimulationsToPlot[sim];

    plot = 
    If[sims === {}, $nullPlot,

    progress = Map[(readProgress[#])&, sims];

    have = Map[Length[#] > 0 &, progress];
    progress = Pick[progress, have];
    labels = Pick[labels, have];

    progressPlot = PresentationListLinePlot[progress, plotOptions, PlotRange -> {0, All},
      FrameLabel -> {"Day", "t/M"},
      PlotLegend -> labels,
      AspectRatio -> 1,
      PlotLabel -> "Progress"]];

    {<|"Filename" -> FileNameJoin[{"web-simulations",sim,"progress.svg"}], "Plot" -> plot,
      "Title" -> "Progress"|>}];
