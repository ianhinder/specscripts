initialDataPlots[sim_String] :=
  Module[{sims, idSubSimNames, idErrs, idErrsAvail, plot},
    
    Print["id.svg: ", sim];

    sims = 
    If[StringMatchQ[sim, ___~~"Ecc"~~_~~EndOfString],
      {sim},
      FileNames["Ecc*", FindSpECSimulation[sim]]];

    idSubSimNames =
    Map[StringReplace[#, __ ~~ sim ~~ "/" -> ""] &, sims];

    idErrs = ReadSpECInitialDataErrors /@ sims;
    idErrsAvail = Pick[idErrs, Length[#] > 0 & /@ idErrs];
    idSubSimNames = Pick[idSubSimNames, Length[#] > 0 & /@ idErrs];

    plot = PresentationListLinePlot[Log10/@idErrs,
      plotOptions,
      FrameLabel -> {"iteration", "log10 ||E||"},
      PlotLegend -> idSubSimNames, PlotLabel -> "Initial data",
      AspectRatio -> 1];

    {{FileNameJoin[{"web-simulations",sim,"id.svg"}], plot}}];
