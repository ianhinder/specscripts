
(*********************************************************************
  Speeds
 *********************************************************************)

speedPlots[sim_String] :=
  Module[{sims, plot, labels, speed, have},

    Print["speed.svg: ", sim];

    {labels,sims} = findSimulationsToPlot[sim];

    plot = 
    If[sims === {}, $nullPlot,
    
    speeds = Map[(ReadSpECSimulationSpeed[#])&, sims];

    have = Map[Length[#] > 0 &, speeds];
    speeds = Pick[speeds, have];
    labels = Pick[labels, have];

    PresentationListLinePlot[speeds, plotOptions, PlotRange -> {{0,All},{0, All}},
      FrameLabel -> {"t/M",
        "dt/dT / (M \!\(\*SuperscriptBox[\(hr\), \(-1\)]\))"},
      PlotLegend -> labels, LegendPosition -> {Right, Top},
      AspectRatio -> 1,
      PlotLabel -> "Evolution speed"]];

    {<|"Filename" -> FileNameJoin[{"web-simulations",sim,"speed.svg"}], "Plot" -> plot,
      "Title" -> "Speed"|>}];
