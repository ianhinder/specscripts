
(*********************************************************************
  Cores
 *********************************************************************)

corePlots[sim_String] :=
  Module[{sims, plot, labels, cores},

    Print["cores.svg: ", sim];

    {labels,sims} = findSimulationsToPlot[sim];

    plot = 
    If[sims === {}, $nullPlot,
    
    cores = Map[(print[#]; ReadSpECCoreCount[#]) &, sims];

    have = Map[Length[#] > 0 &, cores];
    cores = Pick[cores, have];
    labels = Pick[labels, have];

    PresentationListLinePlot[cores, plotOptions, PlotRange -> {0, All},
      FrameLabel -> {"t/M",
      "# of cores"},
      PlotLegend -> labels,
      AspectRatio -> 1,
      PlotLabel -> "Cores"]];

    {{FileNameJoin[{"web-simulations",sim,"cores.svg"}], plot}}];

