(*********************************************************************
  Orbital frequency time derivative
 *********************************************************************)

orbitalFrequencyDotPlots[sim_String] :=
  Module[
    {plot, labels, sims, oms, oms2, labels2, have, omDots},

    {labels,sims} = findSimulationsToPlot[sim];

    plot = 
    If[sims === {},
       $nullPlot,
       (* else *)

       oms = ReadSpECOrbitalOmega /@ sims;
       have = Map[Length[#] =!= 0 &, oms];
       oms2 = Pick[oms, have];
       labels2 = Pick[labels, have];
       omDots = NDerivative[1] /@ Norm /@ oms2;

       PresentationListLinePlot[omDots, plotOptions,
         PlotRange -> {{0, All}, (*{0,1}10^-5*) Automatic},
         PlotLegend -> labels2, PlotLabel-> "Orbital frequency time derivative",
         AspectRatio -> 1,
         FrameLabel ->{"t/M", "|M^2 omDot|"}]];

    {<|"Filename" -> FileNameJoin[{"web-simulations",sim,"omDot.svg"}], "Plot" -> plot,
      "Title" -> "Orbital frequency time derivative"|>}];
