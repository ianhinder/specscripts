(*********************************************************************
  Subdomains
 *********************************************************************)

subdomainPlots[sim_String] :=
  Module[{sims, plot, labels, memory, triggerTimes},

    Print["subdomains.svg: ", sim];

    {labels,sims} = findSimulationsToPlot[sim];

    plot = 
    If[sims === {}, $nullPlot,
    
    subdomains = Map[(ReadSpECSubdomains[#]) &, sims];
    have = Map[Length[#] > 0 &, subdomains];
    subdomains = Pick[subdomains, have];
    labels = Pick[labels, have];

    triggerTimes = ReadSpECAMRTriggerTimes /@ sims;

    PresentationListLinePlot[subdomains, plotOptions, PlotRange -> {0, All},
      FrameLabel -> {"t/M",
      "# of subdomains"},
      PlotLegend -> labels,
      AspectRatio -> 1,
      GridLines -> {triggerTimes[[1]], None},
      PlotLabel -> "Subdomains"]];

    {{FileNameJoin[{"web-simulations",sim,"subdomains.svg"}], plot}}];
