
(*********************************************************************
  GridPoints
 *********************************************************************)

gridpointsPlots[sim_String] :=
  Module[{sims, plot, labels, memory},

    Print["gridpoints.svg: ", sim];

    {labels,sims} = findSimulationsToPlot[sim];

    plot = 
    If[sims === {}, $nullPlot,
    
    gps = Map[((*print[#];*) ReadSpECGridPoints[#]) &, sims];

    have = Map[Length[#] > 0 &, gps];
    gps = Pick[gps, have];
    labels = Pick[labels, have];

    PresentationListLinePlot[gps, plotOptions, PlotRange -> {0, All},
      FrameLabel -> {"t/M",
      "# of gridpoints"},
      PlotLegend -> labels,
      AspectRatio -> 1,
      PlotLabel -> "Gridpoints"]];

    {{FileNameJoin[{"web-simulations",sim,"gridpoints.svg"}], plot}}];
