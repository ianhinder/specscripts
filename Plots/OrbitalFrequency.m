(*********************************************************************
  Orbital frequency
 *********************************************************************)

orbitalFrequencyPlots[sim_String] :=
  Module[
    {plot, labels, sims, oms, oms2, labels2, have},

    {labels,sims} = findSimulationsToPlot[sim];

    plot = 
    If[sims === {},
       $nullPlot,
       (* else *)

       oms = ReadSpECOrbitalOmega /@ sims;
       have = Map[Length[#] =!= 0 &, oms];
       oms2 = Pick[oms, have];
       labels2 = Pick[labels, have];

       PresentationListLinePlot[Log10/@Abs/@oms[[All,3]], plotOptions,
         PlotRange -> {{0, All}, {-2,0}},
         PlotLegend -> labels2, PlotLabel-> "Orbital frequency",
         AspectRatio -> 1,
         FrameLabel ->{"t/M", "log10 |M om|"}]];

    {<|"Filename" -> FileNameJoin[{"web-simulations",sim,"om.svg"}], "Plot" -> plot,
      "Title" -> "Orbital frequency"|>}];
