(*********************************************************************
  Memory
 *********************************************************************)

memoryPlots[sim_String] :=
  Module[{sims, plot, labels, memory},

    Print["memory.svg: ", sim];

    {labels,sims} = findSimulationsToPlot[sim];

    plot = 
    If[sims === {}, $nullPlot,
    
    memory = Map[(ReadSpECMaxEffectiveUsedMemoryFraction[#]) &, sims];

    have = Map[Length[#] > 0 &, memory];
    memory = Pick[memory, have];
    labels = Pick[labels, have];

    (* TODO: get the max memory from simulation metadata *)
    PresentationListLinePlot[memory, plotOptions, PlotRange -> {0, 1.1},
      FrameLabel -> {"t/M",
      "\!\(\*SubscriptBox[\(\[ScriptCapitalM]\), \
\(used\)]\)/\!\(\*SubscriptBox[\(\[ScriptCapitalM]\), \(total\)]\)"},
      PlotLegend -> labels,
      LegendPosition -> {Right, Top},
      AspectRatio -> 1,
      GridLines -> {None, {0.9,1}},
      PlotLabel -> "Memory"]];

    {{FileNameJoin[{"web-simulations",sim,"memory.svg"}], plot}}];
