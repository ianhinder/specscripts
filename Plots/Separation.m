(*********************************************************************
  Separation
 *********************************************************************)

separationPlots[sim_String] :=
  Module[
    {plot, labels, sims, seps, seps2, labels2, have},

    Print["sep.svg: ", sim];

    {labels,sims} = findSimulationsToPlot[sim];

    plot = 
    If[sims === {},
       $nullPlot,
       (* else *)
       seps = Norm/@readHorizon/@sims;
       have = Map[Length[#] =!= 0 &, seps];
       seps2 = Pick[seps, have];
       labels2 = Pick[labels, have];

       PresentationListLinePlot[seps2, plotOptions, PlotRange -> {{0, All}, {0,All}},
                                PlotLegend -> labels2, PlotLabel-> "Coordinate separation",
                                AspectRatio -> 1,
                                FrameLabel ->{"t/M", "D/M"}]];

    {<|"Filename" -> FileNameJoin[{"web-simulations",sim,"sep.svg"}], "Plot" -> plot,
      "Title" -> "Separation"|>}];
