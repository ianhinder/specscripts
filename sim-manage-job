#!/bin/bash

set -e
set -u

sim=$1
lev=$2

shift 2
args="${@}"

set -o pipefail
shopt -s nullglob

source ${BASH_SOURCE%/*}/sim-functions.sh

status=$(./sim-status $sim $lev)
if [ "$status" = "U" ]; then
    term_state="$(sim_termination_state $sim $lev)"
    if [ "$term_state" = "NotYetRun" ]; then
        ./sim-cleanup $sim $lev
    fi
    if [ "$term_state" = "NotYetRun" -o "$term_state" = "Continue" ]; then
        simpath=$(set -e; find_sim_dir $sim)
        last_seg=$(set -e; find_last_sim_segment $sim $lev)
        seg_path=$simpath/$last_seg
        current_cores=$(cd $seg_path; bin/MakeSubmit.py query Cores)
        (cd $seg_path/Run; ../bin/MakeSubmit.py ${args[@]})
    else
        echo "Simulation $sim $lev has termination state $term_state - cannot manage job" >&2
        exit 1
    fi
else
    echo "Simulation $sim $lev has job status $status - cannot manage job" >&2
    exit 1
fi
