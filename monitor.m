
(*********************************************************************
  Set up command line tool
  *********************************************************************)

SetOptions[$Output, FormatType -> OutputForm];
SetOptions["stdout", PageWidth -> Infinity];

$Path = Join[{"~/SpEC/Testing", "~/projects"}, $Path];

Get["SimulationTools`"];

AbortOnMessagesST[True];

Quiet[FindRoot[x^2 == 0, {x, 5, 2, 7}], FindRoot::reged];

Get["MonitorFunctions.m"];

$SpECSimulationsDirectory = Flatten[StringCases[
  StringSplit[
    Import["specsims.conf", 
           "String"], "\n"], "SIM_BASE_DIR=" ~~ s__ :> s]][[1]];

Print["$SpECSimulationsDirectory = ", $SpECSimulationsDirectory];

(*********************************************************************
  Read list of simulations to process
  *********************************************************************)

{doStatus, sims} = FindMonitorSimulations[];

Print["Requested simulations:"];
Scan[Print, sims];
Print[];


CheckAbort[
  If[doStatus, MonitorSimulations[sims], Scan[MonitorSimulation, sims]],
  Print["Aborted"]; 
  Print["Web reporting process exited with an error at "<>DateString[]<>"."];
  Quit[1]];
