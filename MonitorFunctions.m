
BeginPackage["MonitorFunctions`"];

Get["SimulationTools`"];

FindMonitorSimulations;
EvolutionSimulations;
StatusTable;
PlotsForSimulation;
ExportSVG;
ExportPlot;
MonitorSimulations;
MonitorSimulation;
PlotGenerationError;
SimulationStatus;
StatusTableXML;
WriteStatusTable;
StatusTableSimulations;

Begin["`Private`"];

plotOptions = {ImageSize->200, LabelStyle -> 8};

$nullPlot = " ";

webDestDir = "web-simulations";
webSrcDir = "WebFiles";

WithArgumentChecking["MonitorFunctions`*",

inWebDestDir[f_String] :=
  FileNameJoin[{webDestDir,f}];

inWebSrcDir[f_String] :=
  FileNameJoin[{webSrcDir,f}];

import[file_String, args___] :=
  If[FileExistsQ[file],
    Import[file,args],
    Error["import: file "<>file<>" not found"]];

Module[
  {plotScripts = FileNames["*.m", FileNameJoin[
    {FileNameDrop[$InputFileName,-1],"Plots"}]]},
  Do[
    Print["Loading ", script];
    Get[script],
    {script, plotScripts}]];

readSpECOrbits[sim_String] :=
  Module[{horizons, trajectories, phases},
    (* Print["Reading orbits for ", sim]; *)
    horizon = ReadSpECHorizonCentroid[sim,1]-ReadSpECHorizonCentroid[sim,2];
    trajectory = Thread[ToListOfData /@ horizon[[1 ;; 2]]];
    phase = Phase[horizon[[1]]+I horizon[[2]]];
    If[Length[phase] === 0, Print["No data; returning phase of 0"]; 0, Last[phase]/(2.*Pi)]];

readHorizon[sim_String] := readHorizon[sim] = 
  ReadSpECHorizonCentroid[sim,1]-ReadSpECHorizonCentroid[sim,2];

readPsi4[sim_String] := readPsi4[sim] =
  ReadSpECPsi4[sim, 2, 2, "0100"];

readStrain[sim_String] := readStrain[sim] =
  ReadSpECStrain[sim, 2, 2, "0100"];

readProgress[sim_String] := readProgress[sim] = 
  ReadSpECSimulationProgress[sim];


FixSVGDimensions[file_String, file2_String] :=
  RunSubprocess[{"./fix-svg.py", file, file2}, Exceptions -> True];

ExportSVG[file_String, obj_] :=
  Module[{},
    Print["Exporting ", file];
    (* Export[file, obj]; *)

    (* This Export/Import trick improves the appearance of text,
       including font weight and word spacing *)
    Export[file, First[ImportString[ExportString[obj, "PDF"],"PDF"]], "SVG"]
    (* FixSVGDimensions[file,file] *)
];

(*********************************************************************
  Status
 *********************************************************************)

specSimStatus[sim_String] :=
Module[{runBase,res,status},
  {runBase, res} = StringSplit[sim, ":"];
  status=ReadList["!./sim-status "<>runBase<>" "<>res, String];
  If[Length[status] =!= 1, Print["WARNING: Error determining status of simulation ", sim, "\nOutput was ", ToString[status]];
    Return["E"]];
  status[[1]]];


specSimStatus2[sim_String] :=
  Replace[specSimStatus[sim],{
    "U" :> 
    Module[{problem=specSimProblem[sim]},
      Which[
        MemberQ[{"FinalTime"(*,"EccentricityReduction","EccentricityReductionContinuation"*)},problem], "F",
        problem === "Paused", "P",
        True,"U"]]}];

specSimProblem[sim_String] :=
Module[{runBase,res},
  {runBase, res} = StringSplit[sim, ":"];
  ReadList["!./sim-problem "<>runBase<>" "<>res, String][[1]]];

formatStatus[s_String] :=
  Replace[s,
    {"R" :> Style["Running",Darker@Green],
     "Q" :> Style["Waiting", Gray],
     "U" :> Style["Stopped",Darker@Red],
     "F" :> Style["Finished", Darker@Blue],
     "D" :> Style["Deletion", Darker@Red],
     _   :> Style["[Error]", Red]}];

(* formatStatusXML[s_String] := *)
(*   Replace[s, *)
(*     {"R" :> XMLElement["state", {"class"->"running"}, {"Running"}], *)
(*      "Q" :> XMLElement["state", {"class"->"waiting"}, {"Waiting"}], *)
(*      "U" :> XMLElement["state", {"class"->"stopped"}, {"Stopped"}], *)
(*      "F" :> XMLElement["state", {"class"->"finished"}, {"Finished"}], *)
(*      "D" :> XMLElement["state", {"class"->"stopped"}, {"Deletion"}], *)
(*      _   :> "(error)"}]; *)

formatStatusXML[s_String] :=
  Replace[s,
    {"R" :> "Running",
     "Q" :> "Waiting",
     "U" :> "Stopped",
     "F" :> "Finished",
     "D" :> "Deletion",
     "P" :> "Paused",
     _   :> "(error)"}];

lastOrNone[l_] := If[Length[l] === 0, None, Last[l]];

StatusTable[sims_List] :=
  Module[{walltimes, totalWalltimes, progress, ages, statuses, orbits, statusTable, progressTimes, orbitSpeeds, statusTableHTML, element},

    Print["status.html"];

    walltimes = Map[(ReadSpECWallTime[#])&, sims];

    totalWalltimes = lastOrNone/@walltimes;

    progress = Map[(readProgress[#])&, sims];

    ages = Map[
      If[Length[#] > 0,
      (* TODO: use the current year *)
      (DateDifference[DatePlus[DateList[{2015, 1, 1, 0, 0, 0}], MaxCoordinate[#]],
      DateList[], "Hour"][[1]]),
        None]&, progress];

    statuses = formatStatus/@Map[Replace[specSimStatus[#], {"U" :> If[MemberQ[{"FinalTime","EccentricityReduction","EccentricityReductionContinuation"}, specSimProblem[#]], "F", "U"]}] &, sims];

    orbits = readSpECOrbits /@ sims;

    orbitSpeeds = orbits/(totalWalltimes/24); (* Orbits per day *)

    formatSimName[n_String] :=
    StringReplace[n,$SpECSimulationsDirectory<>"/"->""];

    mainSim[n_String] :=
    StringReplace[n,{":"~~__->"","/Ecc"~~_->"",StartOfString~~"/"->""}];

    addLink[n_String] :=
    "<a href = \"" <> mainSim[n]<>"/index.xml" <> "\">" <> n <> "</a>";

    statusTableHTML = Thread[{
      Prepend[addLink/@Map[formatSimName,sims],"Simulation"],
      Prepend[statuses,"Status"],
      Prepend[If[#===None,"",NumberForm[N@#,{Infinity,1}]] & /@ totalWalltimes,"Run time/hrs"],
      Prepend[If[#===None,"",NumberForm[N@#/24,{Infinity,1}]] & /@ (totalWalltimes),"Run time/days"],
      Prepend[NumberForm[Chop[#,10^-1],{Infinity,1}] & /@ orbits,"# Orbits"],
      Prepend[NumberForm[Chop[#,10^-1],{Infinity,1}] & /@ orbitSpeeds,"Orbits per day"],
      Prepend[If[#===None, "", NumberForm[N@#,{Infinity,1}]] & /@ ages,"Age/hrs"],
      Prepend[MapThread[If[#1[[1]]==="Stopped", specSimProblem[#2], ""] &, {statuses, sims}],"Problem"]
                                              }];

    element[el_, content_]:=
    StringJoin[{"<"<>el<>">", StringJoin[content], "</"<>el<>">"}];

    statusTableHTML = statusTableHTML /. "Stopped" -> "<span class=\"stopped\">Stopped</span>";
    statusTableHTML = statusTableHTML /. "Finished" -> "<span class=\"finished\">Finished</span>";
    statusTableHTML = statusTableHTML /. "Running" -> "<span class=\"running\">Running</span>";

    WriteString[inWebDestDir["status.html"],
      element["table",
        Join[
          Map[element["tr", #] &,
          Map[element["th", ToString[#]] &,
            statusTableHTML[[1;;1]], {2}]],

        Map[element["tr", #] &,
          Map[element["td", ToString[#]] &,
            statusTableHTML[[2;;All]], {2}]]]]];

    Print["Finished generating status table"];
        ];

StatusTableXML[sims_List] :=
  Module[{fields, statuses},
    Print["Generating status table (2)"];
    statuses = SimulationStatus /@ sims;

    fields = statuses[[1,3,All,1]] /. {
      "name" -> "Simulation",
      "status" -> "Status",
      "runtimehours" -> "Run time/hrs",
      "runtimedays" -> "Run time/days",
      "orbits" -> "# orbits",
      "orbitsperday" -> "Orbits per day",
      "agehours" -> "Age/hrs",
      "problem" -> "Problem"};

    XMLObject["Document"][
      {XMLObject["ProcessingInstruction"][
        "xml-stylesheet", "href=\"status.xsl\" type=\"text/xsl\""]},
      XMLElement["simulations", {},
        Join[{XMLElement["fields", {}, XMLElement["field", {}, {#}]& /@ fields]},
          statuses]], {}]];

WriteStatusTable[sims_List, dir_String] :=
  Module[{},
    Export[dir<>"/status.xml",StatusTableXML[sims]];
    forceCopy[inWebSrcDir["status.xsl"], dir<>"/status.xsl"]];

formatSimName[n_String] :=
  StringReplace[n,$SpECSimulationsDirectory<>"/"->""];

mainSim[n_String] :=
  StringReplace[n,{":"~~__->"","/Ecc"~~_->"",StartOfString~~"/"->""}];

addLink[n_String] :=
  "<a href = \"" <> mainSim[n]<>"/index.xml" <> "\">" <> n <> "</a>";

SimulationStatus[sim_String] :=
  Module[{walltime, totalWalltime, progress, age, status, orbits, statusTable,
    orbitSpeed, problem},
    Print["Reading status of "<>sim];
    walltime = ReadSpECWallTime[sim];
    totalWalltime = lastOrNone[walltime];
    progress = readProgress[sim];

    age =
      If[Length[progress] > 0,
      (* TODO: use the current year *)
      (DateDifference[DatePlus[DateList[{2015, 1, 1, 0, 0, 0}], MaxCoordinate[progress]],
      DateList[], "Hour"][[1]]),
        None];

    status = specSimStatus2[sim];
    problem = If[status === "U", specSimProblem[sim], ""];

    XMLElement["simulation",{}, {
      XMLElement["name", {"href"->mainSim[formatSimName[sim]]<>"/index.xml"}, {formatSimName[sim]}],
      XMLElement["status", {}, {formatStatusXML@status}],
      XMLElement["runtimehours", {}, {If[totalWalltime===None,"",ToString@NumberForm[totalWalltime,{Infinity,1}]]}],
      XMLElement["runtimedays", {}, {If[totalWalltime===None,"",ToString@NumberForm[totalWalltime/24,{Infinity,1}]]}],
      XMLElement["orbits", {}, {ToString[NumberForm[orbits=N@readSpECOrbits[sim],{Infinity,1}]]}],
      XMLElement["orbitsperday", {}, {ToString[NumberForm[N@orbits/(totalWalltime/24),{Infinity,1}]]}],
      XMLElement["agehours", {}, {If[age===None, "", ToString@NumberForm[N@age,{Infinity,1}]]}],
      XMLElement["problem", {}, {problem}]}]];

plotInformation[dir_String] :=
  Module[{plotInfos},
    plotInfos = Cases[import[dir<>"/index.xml"],
      XMLElement["plot", _, {
        XMLElement["filename",_,{filename_}],
        XMLElement["title",_,{title_}]}] :>
      {filename,title}, Infinity];
    <|"Filename" -> #[[1]], "Title" -> #[[2]]|> & /@ plotInfos

];

GenerateSummaryPlotXML[sims_List, dir_] :=
  Module[{plotInfos, assocs, nameTitles, xmlFiles},
    plotInfos = Map[plotInformation[FileNameJoin[{dir,#}]] &, sims];

    assocs = DeleteDuplicates[Select[Flatten[plotInfos,1], #["Title"] =!= "" &]];
    (* assocs = Select[plots,AssociationQ]; *)
    nameTitles = Union@Map[{FileNameTake[#["Filename"], -1], #["Title"]} &, assocs];
    xmlFiles = StringReplace[#,".svg"->".xml"] & /@ nameTitles[[All,1]];

    MapThread[
      Export[dir<>"/"<>#3, 
        XMLObject["Document"][
          {XMLObject["ProcessingInstruction"][
            "xml-stylesheet", "href=\"summaryplots.xsl\" type=\"text/xsl\""]},
          XMLElement["plots", {},
            {XMLElement["title", {},{#2}], XMLElement["plotfile", {},{#1}]}],
          {}]] &, {nameTitles[[All,1]], nameTitles[[All,2]], xmlFiles}];

    Export[dir<>"/summaryplots.xml", 
      XMLObject["Document"][
        {XMLObject["ProcessingInstruction"][
          "xml-stylesheet", "href=\"summaryplots.xsl\" type=\"text/xsl\""]},
        XMLElement["summaryplots", {},
          Table[XMLElement["plotpage", {"href"->f},{}],{f, xmlFiles}]],
        {}]]];

GenerateRemainingTimePlot[sims_, destDir_] :=
  Module[{plot},
    Print["Generating remaining time plot"];
    plot = PlotSpECSimulationsRemainingTime[sims];
    ExportSVG[destDir<>"/remaining.svg",plot]];

errorBox[value_] :=
  Module[{msg,stack},
    Return[Graphics[Text["Error"]]];
    msg = value[[1]];
    stack = value[[3]](*[[2;;-2]]*);
    Framed[Pane[
      Style["Error: "<>msg<>"\nin "<>StringJoin[Riffle[ToString/@stack,"/\n   "]],
        FontColor->Darker[Red], FontWeight->Bold,FontFamily->"Sans",
        LineIndent -> 0, TextAlignment -> Left],
      ImageSize->{200,200}]]];

withoutContext[n_String] := StringReplace[n, __~~"`"~~x__ :> x];

generatePlot[sim_String, plotFn_] :=
  Module[{timeout = 120},
    WithExceptionsFn[
      TimeConstrained[
        Print[withoutContext@ToString[plotFn]];
        plotFn[sim], timeout, 
        With[{message = "Timeout while generating plot "<>withoutContext@ToString[plotFn]<>" for "<>sim},
          Print[message];
          Sow[message, PlotGenerationError]];
        {}],
      CaughtMessageException -> 
      Function[{value,tag},
        errIndex=errIndex+1;
        Sow["Error occurred when generating plot for "<>sim<>" - "<>ToString[value], PlotGenerationError];
        {{FileNameJoin[{webDestDir,sim,"Error"<>ToString[errIndex]<>".svg"}],
          errorBox[value]}}]]];

ExportPlot[sim_String, plotFn_, filename_ : Automatic] :=
  Module[{plots,filename2,plot},
    plots = generatePlot[sim,plotFn];
    filename2 = If[filename === Automatic, plots[[1]]["Filename"], filename];
    plot = plots[[1]]["Plot"];
    ExportSVG[FileNameTake[filename2,-1],plot]];

PlotsForSimulation[sim_String] :=
  Module[{plots, plotFunctions, errIndex=0, validPlotListQ},
    Print["Generating plots for ", sim];

    plotFunctions = {
      initialDataPlots,
      parameterPlots,
      subdomainPlots,
      gridpointsPlots,
      corePlots,
      eccentricityReductionPlots,
      memoryPlots,
      speedPlots,
      progressPlots,
      trajectoryPlots,
      waveformPlots,
      waveFrequencyPlots,
      waveFrequencyDotPlots,
      separationPlots,
      orbitalFrequencyPlots,
      orbitalFrequencyDotPlots,
      orbitalFrequency3DotPlots,
      phaseErrorPlots};

    plots = Map[generatePlot[sim,#] &, plotFunctions];

    validPlotListQ[x_] :=
    (MatchQ[x, {{ _String, (_Graphics | $nullPlot)}...}] || 
      (AssociationQ[x[[1]]] (*&&
        Complement[{"Filename", "Plot"}, Keys[x[[1]]]] === {}*)));

    Scan[If[!validPlotListQ[#], Put[#, "invalid-plot.m"]; Error["Invalid plot"]] &, plots];
    Join@@plots];

expandSimulation[sim_String] :=
  Module[{subs},
    subs = FindSpECSubSimulations[sim];
    If[MatchQ[subs,{_String...}] === False,
      Print["Error getting subsimulations for "<>sim<>". Returned "<>ToString[subs]];
      Quit[1]];
    If[subs === {},
      {sim},
      Prepend[Flatten[Map[expandSimulation, subs],1],sim]]];

EvolutionSimulations[sim_String] :=
  Module[{expanded},
    expanded = expandSimulation[sim];
    Flatten[
      StringCases[expanded,
        x:(StartOfString~~__)~~"/Ev/Lev"~~l_~~"_AA"~~EndOfString :> x<>":"<>l]]];

StatusTableSimulations[sim_String] :=
  Module[{eccSims},

    (* sim is a top-level simulation.  It may have eccentricity
    reduction subsimulations, or it may have initial data and possibly
    resolution levels. *)

    eccSims = FindSpECEccentricityReductionSimulations[sim];

    If[eccSims === {},
      Module[{levs},
        levs = FindSpECLevels[sim];
        If[levs === {},
          (* Have no resolutions *)
          Replace[FindSpECInitialDataSimulations[sim], {
            {} :> (Print["WARNING: Simulation "<>sim<>" has no initial data directory"];{}),
            {id_} :> (*id*){}, (* TODO: support ID simulations in the status table *)
            _ :> Error["Found multiple initial data simulations in "<>ToString[sim]]}],
          (* Have some resolutions *)
          Map[sim<>":"<>ToString[#] &, levs]]],
      StatusTableSimulations[Last[eccSims]]]
];

EccentricityReductionIteration[sim_String] :=
  Module[{i},
    is = StringCases[sim, "/Ecc"~~i:(DigitCharacter..) :> ToExpression[i]];
    Replace[is, {
      {} :> None,
      {x_} :> x,
      _ :> Error["Cannot determine a unique eccentricity reduction iteration for simulation "<>sim<>". Obtained "<>ToString[x]<>"."]}]];
      
(* Given a list of simulation names, return the subset which are the
   last eccentricity reduction in each *)
LastEccentricityIteration[sims_List] :=
  Map[Last,SplitBy[sims, EccentricityReductionIteration]];

initialDataSimulations[sim_String] :=
  Module[{expanded},
    expanded = expandSimulation[sim];
    ShowIt[expanded];
    Flatten[
      StringCases[expanded,StartOfString ~~ __ ~~ "/ID"]]];

findEccentricityReductionSimulationResolutions[sim_String] :=
  Module[{eccRedSims,levels,have,simRess,simRess2},
    eccRedSims = FindSpECEccentricityReductionSimulations[sim];
    levels = FindSpECLevels/@eccRedSims;
    have = Map[Length[#]>0&, levels];
    eccRedSims = Pick[eccRedSims, have];
    levels = Pick[levels, have];
    simRess = MapThread[
      StringJoin[#1, ":", #2] &, {eccRedSims,
        Map[ToString[Last[#]]&, levels]}];

    simRess2 = Flatten[MapThread[Map[Function[lev, StringJoin[#1, ":", ToString[lev]]], #2] &, {eccRedSims, levels}],1];
    simRess2];

findSimulationsToPlot[sim_String] :=
  Module[{levels,sims,eccRedSims},
    levels = FindSpECLevels[sim];
    sims =
    If[levels =!= {},
      (* There are no evolution levels *)
      {levels,Map[sim<>":"<>ToString[#] &,levels]},
      (* else *)
      (* We must still be in eccentricity-reduction *)
      eccRedSims = findEccentricityReductionSimulationResolutions[sim];
      If[eccRedSims =!= {},
        {(*(StringSplit[FileNameSplit[#][[-1]],":"][[1]])*) (FileNameSplit[#][[-1]]) &  /@ eccRedSims, eccRedSims},
        {{},{}}]]];

subtract[d1_DataTable, d2_DataTable] :=
  Module[{f1,f2, range},
    f1 = Interpolation[d1];
    f2 = Interpolation[d2];
    range = CommonInterval[{d1,d2}];
    ToDataTable@Table[{t, f1[t]-f2[t]}, {t, range[[1,1]], range[[1,2]], 1}]];

print[args___] :=
  WriteString["stdout", StringJoin[ToString/@{args}]];

pad[l_List] := pad/@l;

pad[g_Graphics] :=
  Show[g, ImagePadding->{{64,2},{10,5}}];

CommandLineArguments[] :=
  If[$Notebooks, {},
    Replace[$CommandLine, {___,"-script",_,rest___} :> {rest}]];

SpECSimulationQ[dir_String] :=
  FileNames["Ecc*", dir] =!= {} || FileNames["DoMultipleRuns.input", dir] =!= {};

FindSpECSimulationsInDirectory[dir_String] :=
  Module[{all, dirs, sims},
    all = FileNames["*", ShowIt@dir];
    dirs = Select[ShowIt@all, FileType[#] === Directory &];
    sims = Select[ShowIt@dirs, SpECSimulationQ];
    ShowIt@sims];

FindMonitorSimulations[] :=
  Module[{sims, args, status},
    args = CommandLineArguments[];
    {status,sims} = 
    If[args === {},
      With[{file="monitor-sims.m"},
        If[FileExistsQ[file],
          {True,Get[file]},
          {True, FindSpECSimulationsInDirectory[$SpECSimulationsDirectory]}]],
      (* else *)
      {False, args}];
    sims = Select[sims, !FileExistsQ[FileNameJoin[{#,"cancelled"}]] &];
    sims = Map[StringReplace[#, $SpECSimulationsDirectory<>"/"->""] &, sims];
    {status, sims}];

forceCopy[a_String,b_String] :=
  Module[{},
    If[FileExistsQ[b], DeleteFile[b]];
    CopyFile[a,b]];

shortenString[s_String]:=
  Module[{maxLen = 400, sep = " <...> ", preLen, postLen},
  If[StringLength[s] > maxLen,
    preLen = Floor[maxLen/2]; postLen = Floor[maxLen/2];
    StringTake[s,preLen]<>sep<>StringTake[s,-postLen],
    s]];

plotXML[plot_] :=
  Module[{fileName = FileNameDrop[First[plot],2], title = If[AssociationQ[plot], plot["Title"], ""]},
    XMLElement["plot",{},{XMLElement["filename",{},{fileName}],
      XMLElement["title",{},{title}]}]];

plotsXML[plots_List] :=
Module[{},
  XMLElement["plots",{},
    plotXML/@plots]];

MonitorSimulation[sim_String] :=
  Module[{plots, errs, nullPlots, realPlots, header, xml, startDate},

  (*********************************************************************
    Generate plots
    *********************************************************************)

  Print["Creating plots"];
  startDate = DateList[];

  {plots, errs} = Reap[PlotsForSimulation[sim],PlotGenerationError];

  Print["Creating directories for plots"];
  Quiet[CreateDirectory[FileNameDrop[#,-1],CreateIntermediateDirectories->True] & /@ plots[[All,1]], CreateDirectory::filex];

  (*********************************************************************
    Report errors
    *********************************************************************)

  cdata[x_]:=
    "<![CDATA[" <> shortenString@ToString[x] <> "]]>";

  Export[inWebDestDir[sim<>"/errors.xml"],
    Join[{
      "<errors>",
      "<ul>",
      Sequence@@Map["<li>"<>cdata@#<>"</li>" &, ToString/@Flatten[errs]],
      "</ul>",
      "</errors>"}], "Text"];

  (*********************************************************************
    Export plots
    *********************************************************************)

  nullPlots = Select[plots, StringQ[#[[2]]] &];
  realPlots = Select[plots, !StringQ[#[[2]]] &];

  Print["Exporting SVG files of plots"];

  If[Length[realPlots] > 0,
    Scan[ExportSVG[#[[1]], #[[2]]] &, Thread[{realPlots[[All,1]], PadGraphics[realPlots[[All,2]]]}]];
    , (* else *)
    Print["No real plots to export"]];

  If[Length[nullPlots] > 0,
    Scan[ExportSVG[#[[1]], #[[2]]] &, Thread[{nullPlots[[All,1]], nullPlots[[All,2]]}]];
    , (* else *)
    Print["No null plots to export"]];

    Print["Exporting XML for ", sim];

    

    xml = ExportString[XMLElement[
      "simulation", {}, {
        XMLElement["name", {}, {sim}],
        XMLElement["lastupdated", {}, {DateString[startDate]}],
        plotsXML[realPlots](*,
                                                                simulationStatus[sim]*)}], "XML"];
    forceCopy[inWebSrcDir["sim-index.xsl"], inWebDestDir[sim<>"/index.xsl"]];
    forceCopy[inWebSrcDir["monitoring.css"], inWebDestDir[sim<>"/monitoring.css"]];

  header = "<?xml version='1.0'?>\n<?xml-stylesheet type=\"text/xsl\" href=\"index.xsl\"?>\n";
    Export[inWebDestDir[sim<>"/index.xml"], StringJoin[header, xml], "String"];

        ];






























MonitorSimulations[sims_, dest_ : Automatic] :=
  Module[{requiredFiles, missingFiles, startDate,
    plots, nestedSims, tableSims, nullPlots, realPlots, header, xml,
    webFiles},

  If[dest =!= Automatic, webDestDir = dest];

  (*********************************************************************
    Check for all required files
    *********************************************************************)

  requiredFiles = {(*"monitor-sims.m",*) "specsims.conf"};

  missingFiles = Select[requiredFiles, !FileExistsQ[#] &];

  If[missingFiles =!= {},
    Print["ERROR: Missing required files/directories"];
    Scan[Print["  "<>#] &, missingFiles];
    Quit[1]];

  startDate = DateList[];

  (*********************************************************************
    Load plotting functions
    *********************************************************************)

  plots = {};
  $SpECVerbosePrint = True;
  tableSims = Flatten[Map[StatusTableSimulations, sims],1];

  Print["Simulations for plotting:"];
  Scan[Print, sims];
  Print[];

  Print["Simulations for status table:"];
  Scan[Print, tableSims];
  Print[];

  If[!DirectoryQ[webDestDir], CreateDirectory[webDestDir]];

  (*********************************************************************
    Write remaining time plot
    *********************************************************************)

  GenerateRemainingTimePlot[tableSims, webDestDir];
  Print["Finished GenerateRemainingTimePlot"];

  (*********************************************************************
    Write status table
    *********************************************************************)

  WriteStatusTable[tableSims, webDestDir];

  (*********************************************************************
    Generate plots
    *********************************************************************)

  Print["Creating plots"];
  MonitorSimulation/@sims;

  (*********************************************************************
    Export simulation index
    *********************************************************************)

  header = "<?xml version='1.0'?>\n<?xml-stylesheet type=\"text/xsl\" href=\"index.xsl\"?>\n";

  xml = ExportString[XMLElement[
    "simulations", {}, Map[XMLElement["simulation", {}, {#}] &, sims]], "XML"];

  Export[inWebDestDir@"index.xml", StringJoin[header, xml], "String"];

  (*********************************************************************
    Generate summary plot XML files
    *********************************************************************)

  GenerateSummaryPlotXML[sims, webDestDir];

  (*********************************************************************
    Copy required WebFiles
    *********************************************************************)

  webFiles = {".htaccess", "index.xsl", "progress.xml", "progress.xsl", 
    "monitoring.css", "summaryplots.xsl"};
  Map[forceCopy[inWebSrcDir[#], inWebDestDir[#]] &, webFiles];

  If[FileExistsQ["config.xml"],
    forceCopy["config.xml", inWebDestDir["config.xml"]]];

  Print[];
  Print["Done"];
  Export[inWebDestDir@"monitor-problems.xml",
    "<monitorproblems></monitorproblems>", "String"]];
                    ];

  End[];

  EndPackage[];
