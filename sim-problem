#!/bin/bash
set -e

sim="$1"
lev="$2"
keywords="$3"

source ${BASH_SOURCE%/*}/sim-functions.sh

simpath=$(find_sim_dir $sim)

id_pattern='/ID$'
evdir=$simpath/Ev
if [[ "$simpath" =~ $id_pattern ]]; then
    if [ -r $simpath/EvID/ID_Params.perl ]; then
        echo "Finished"
    else
        echo "Unknown"
    fi
    exit 0
elif [ ! -d $evdir ]; then
    echo "Cannot find simulation evolution directory $evdir">&2
    exit 1
fi

cd $evdir
last_lev=$(ls -d Lev${lev}_?? Lev${lev}_Ringdown/Lev${lev}_?? 2>/dev/null|tail -n 1)
if [ -z "$last_lev" ]; then
    echo "Error while looking for last segment in $evdir Lev$lev" >&2
    exit 1
fi

outfile=$last_lev/Run/SpEC.out

stdoutfiles=(Lev${lev}_*/SpEC.stdout Lev${lev}_Ringdown/Lev${lev}_*/SpEC.stdout)
stdoutfile=${stdoutfiles[${#stdoutfiles[*]}-1]}

stderrfiles=(Lev${lev}_*/SpEC.stderr Lev${lev}_Ringdown/Lev${lev}_*/SpEC.stderr)
stderrfile=${stderrfiles[${#stderrfiles[*]}-1]}

#echo $PWD >&2

if [ "$keywords" = "cancelled" ]; then
    echo "Cancelled"
elif [ -r ../cancelled ]; then
    echo "Cancelled"
elif [ -r paused_Lev${lev} -o -r Lev${lev}_Ringdown/paused_Lev${lev} ]; then
    echo "Paused"
elif [ -r $outfile ]; then
    stdout_tail="$(tail -1000 $outfile)"
    if grep "Disk quota exceeded" $outfile>/dev/null; then
        echo "DiskQuota"
    elif [[ "$stdout_tail" =~ Error\ detected\ in\ HDF5 && "$stdout_tail" =~ SerialCheckpoint\.h5 ]]; then
        echo RecoverReadFailed
    elif grep "REQUIRE  FAILED" $outfile>/dev/null; then
        req_output="$(awk '/REQUIRE  FAILED/,/#################################################################/' $outfile)"

#        echo "$req_output" >&2

        lock_pattern='Could not re-open held lock on.*TensorYlmDB'

        if [[ (         "${req_output}" =~ 'file_id>=0  violated' ||
                        "${req_output}" =~ 'g_id>=0  violated' ||
                        "${req_output}" =~ 'groupvs>=0  violated' ) &&
                    "${req_output}" =~ DataMeshReaderH5.cpp &&
                    ( "${req_output}" =~ 'Cannot open' ||
                        "${req_output}" =~ 'cannot find label of form' ) ]]; then
            echo "RecoverReadFailed"
        elif [[ "${req_output}" =~ 'ReaderH5.FileExists()  violated' && "${req_output}" =~ "GlobalVarsCheckpointManager.cpp" && "${req_output}" =~ 'Missing filename is' ]]; then
            echo "RecoverReadFailed"
        elif [[ "${req_output}" =~ 'ReaderH5.FileExists()  violated' && "${req_output}" =~ "GlobalVarsCheckpointManagerRemap.cpp" && "${req_output}" =~ 'Cannot find file' ]]; then
            echo "RecoverReadFailed"
        elif [[ "${req_output}" =~ 'H5Dread.*violated' && "${req_output}" =~ "DataMeshReaderH5.cpp" ]]; then
            echo "RecoverReadFailed"
        elif [[ "${req_output}" =~ 'out  violated' ]]; then
            echo "OutputFailed"
        elif [[ "${req_output}" =~ $lock_pattern ]]; then
            echo "TensorYlmDatabaseLockError"
        elif [[ "${req_output}" =~ Failed\ to\ open\ directory.*Checkpoints ]]; then
            echo "OpenCheckpointFailure"
        elif [[ "${req_output}" =~ ProportionalIntegral:\ Desired\ for\ next\ step ]]; then
            echo "OdeController(dt-lt-dtMin)"
        elif [[ "${req_output}" =~ MaximumDt\ .*\ is\ less\ than\ MinimumDt\.* ]]; then
            echo "OdeController(dtMax-lt-dtMin)"
        elif [[ "${req_output}" =~ \#\#\#\#[\ ]*([^\#]*)\ \ violated ]]; then
            echo "UnknownRequire(${BASH_REMATCH[1]})"
        else
            echo "UnknownRequire"
        fi

        # echo "${req_output[1]}"
    # elif grep "Termination condition EccentricityReduction" $outfile >/dev/null; then
        # ecc_status_file=$evdir/JoinedForEcc/EccReduce.status
        # if [ ! -r $ecc_status_file ]; then
        #     echo "EccentricityReductionProblem(StatusFileNotFound)"
        # else
        #     ecc_out=$(cat $ecc_status_file)
        #     if [[ "$ecc_out" =~ Success ]]; then
        #         echo "EccentricityReductionComplete"
        #     elif [[ "$ecc_out" =~ TooSlow ]]; then
        #         echo "EccentricityReductionTooSlow"
        #     elif [[ "$ecc_out" =~ MaxIters ]]; then
        #         echo "EccentricityReductionMaxIters"
        #     elif [[ "$ecc_out" =~ Continue ]]; then
        #         echo "EccentricityReductionContinuation"
        #     else
        #         echo "EccentricityReductionProblem(UnrecognisedStatus)"
        #     fi
        # fi
        # echo "Finished"
    elif grep "Termination condition" <( tail -5 $outfile ) >/dev/null; then
        term_cond=$(grep "Termination condition" $outfile|tail -1|awk '{print $3}')
        echo $term_cond
        # elif grep 'To be continued...' $outfile>/dev/null; then
        #     echo "None"
    elif grep 'ORTE was unable to reliably start one or more daemons' <( tail -100 $outfile ) >/dev/null; then
        echo "FailedMPIRun"
    elif grep 'ORTE has lost communication with its daemon located on node' <( tail -100 $outfile ) >/dev/null; then
        echo "NodeMPIError"
    elif grep '10 (IBV_EVENT_PORT_ERR)' <( tail -1000 $outfile ) >/dev/null; then
        echo "InfinibandPortError"
    else
        echo "Unknown"
    fi
elif [ -r $stderrfile ] && grep "ERROR\|^error:" $stderrfile >/dev/null; then
    # How to stop above condition from matching if the stderr file is "old"?
    if grep "Permission denied" $stderrfile >/dev/null; then
        if [ -r JoinedForEcc ]; then
            echo "NextEccJobSubmissionPermissionDenied"
        else
            echo "JobSubmissionPermissionDenied"
        fi
    else
        echo "JobSubmissionError($evdir/$stderrfile)"
    fi
    exit 0
else
        echo "Unknown"
fi
